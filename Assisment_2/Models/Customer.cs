﻿using System;
namespace Assisment_2
{
	public class Customer
	{
		private string name, address, email;
		private int age, phno,id;
		public int Id
		{
			get
			{
				return id;
			}

			set
			{
				id= value;
			}
		}
		public string Name
		{
			get
			{
				return name;
			}

			set
			{
				name = value;
			}
		}

		public string Address
		{
			get
			{
				return address;
			}

			set
			{
				address = value;
			}
		}

		public string Email
		{
			get
			{
				return email;
			}

			set
			{
				email = value;
			}
		}

		public int Age
		{
			get
			{
				return age;
			}

			set
			{
				age = value;
			}
		}

		public int Phno
		{
			get
			{
				return phno;
			}

			set
			{
				phno = value;
			}
		}
	}
}
