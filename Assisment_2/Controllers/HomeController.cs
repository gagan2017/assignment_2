﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace Assisment_2.Controllers
{
	/*

	  There sshould CRUD opentations to a list.
	  There should be some Ajax calls
	  There should be Partial views.
	  And some styling, using css, some effect using jquery
*/
	public class HomeController : Controller
	{
		public static List<Customer> cList;

		public HomeController()
		{
			if (cList == null)
			{
				cList = new List<Customer>();
				cList.Add(
					new Customer()
					{
						Id = 0,
						Name = "No Name",
						Age = 0,
						Phno = 91,
						Address = "no address",
						Email = "example@gmail.com"
					}
					);
			}


		}


		public ActionResult Index()
		{
			
			Customer c = new Customer();
			return View(c);
		}


		public ActionResult Add()
		{
			return View();
		}
		[HttpPost]
		public ActionResult Add(Customer c)
		{
			c.Id = cList.Count;
			cList.Add(c);
			//cList.Remove(cList[0]);
			return Content("Added");
		}
		public ActionResult Details()
		{
			
			return View(cList);
		}

		public ActionResult Delete(int Id)
		{
			
			Customer cin= cList.Where(x => x.Id == Id && x.Id == Id).FirstOrDefault();

			cList.Remove(cin);

			return PartialView("Deleted "+cin+" Reload");
		}



		public ActionResult Edit(int Id)
		{
			
			Customer S = cList.Where(x => x.Id == Id && x.Id == Id).FirstOrDefault();

			return PartialView("Add",S);

		}
		[HttpPost]
		 public ActionResult Edit(Customer customer)
		{
			foreach (Customer c in cList)
			{
				if (c.Id==customer.Id)
				{
					
					c.Name=customer.Name;
					c.Age = customer.Age;
					c.Email = customer.Email;
					c.Phno = customer.Phno;
					c.Address = customer.Address;

					break;
				}
			}


			return Content("Edited Successfully");

		}


		/*public ActionResult CRUD(string id)
		{
			if (id.Equals("add"))
			{
				return PartialView("Add");
			}
			if (id.Equals("Details"))
			{
				return PartialView("Details");
			}
			else return View();

		}*/




	}
}
